<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Hn.GlossaryOne',
    'Glossary',
    'Glossary'
);

$TCA['tt_content']['columns']['term']['config']['type'] = 'passthrough';