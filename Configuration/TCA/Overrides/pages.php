<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$fields = [
    'tx_glossaryone_exclude_parsing' => [
        'exclude' => 1,
        'label' => 'Exclude this page from parsing',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'tx_glossaryone_exclude_parsing_recursively' => [
        'exclude' => 1,
        'label' => 'Exclude this page and all of its subpages from parsing',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'tx_glossaryone_first_occurence' => [
        'exclude' => 1,
        'label' => 'Replace only the first occurence of a term on this page',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'tx_glossaryone_first_occurence_recursively' => [
        'exclude' => 1,
        'label' => 'Replace only the first occurence of a term on this page and all of its subpages',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'tx_glossaryone_exclude_first_occurence' => [
        'exclude' => 1,
        'label' => 'Do explicitly NOT only replace the first occurence on this page',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $fields);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    '--div--;Glossary,--palette--;Exclude from parsing;tx_glossaryone_exclude,--palette--;Replace only first occurence;tx_glossaryone_first_occurence,'
);

$GLOBALS['TCA']['pages']['palettes']['tx_glossaryone_exclude'] = array(
    'showitem' => 'tx_glossaryone_exclude_parsing,tx_glossaryone_exclude_parsing_recursively'
);
$GLOBALS['TCA']['pages']['palettes']['tx_glossaryone_first_occurence'] = array(
    'showitem' => 'tx_glossaryone_first_occurence,tx_glossaryone_first_occurence_recursively, tx_glossaryone_exclude_first_occurence'
);