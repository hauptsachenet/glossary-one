<?php

return [
    'ctrl' => [
        'title' => 'Term',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'transOrigPointerField' => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'languageField' => 'sys_language_uid',
        'translationSource' => 'l10n_source',
        'searchFields' => 'title, full_title',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group'
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'default' => 0,
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                ],
            ],
        ],
        'l18n_parent' => [
            'exclude' => true,
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0
                    ]
                ],
                'foreign_table' => 'tx_glossaryone_domain_model_term',
                'foreign_table_where' => 'AND tx_glossaryone_domain_model_term.pid=###CURRENT_PID### AND tx_glossaryone_domain_model_term.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'l18n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
                'default' => ''
            ]
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ],
            ],
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'title' => [
            'label' => 'Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim, required'
            ]
        ],
        'full_title' => [
            'label' => 'Full Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'size' => 255,
                'eval' => 'trim'
            ]
        ],
        'abbreviation' => [
            'label' => 'Abbreviation',
            'exclude' => 1,
            'config' => [
                'type' => 'check',
                'default' => '0'
            ]
        ],
        'content' => [
            'label' => 'Content',
            'exclude' => 1,
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tt_content',
                'foreign_field' => 'tx_glossaryone_term',
                'foreign_sortby' => 'sorting',
                'minitems' => 0,
                'maxitems' => 100,
                'behaviour' => [
                    'enableCascadingDelete' => TRUE,
                ],
                'appearance' => [
                    'showPossibleLocalizationRecords' => TRUE,
                    'showAllLocalizationLink' => TRUE,
                    'newRecordLinkTitle' => 'New Content',
                    'collapseAll' => TRUE,
                    'enabledControls' => [
                        'info' => TRUE,
                        'new' => TRUE,
                        'dragdrop' => TRUE,
                        'sort' => TRUE,
                        'delete' => TRUE,
                    ],
                ],
            ]
        ],
        'synonyms' => [
            'exclude' => 1,
            'label' => 'Synonyms',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_glossaryone_domain_model_synonym',
                'foreign_field' => 'term_id',
                'foreign_sortby' => 'sorting',
                'minitems' => 0,
                'maxitems' => 100,
                'behaviour' => [
                    'enableCascadingDelete' => TRUE,
                ],
                'appearance' => [
                    'newRecordLinkTitle' => 'New Synonym',
                    'collapseAll' => TRUE,
                    'enabledControls' => [
                        'info' => TRUE,
                        'new' => TRUE,
                        'dragdrop' => TRUE,
                        'sort' => TRUE,
                        'delete' => TRUE,
                    ],
                ],
            ],
        ],
        'related' => [
            'exclude' => 1,
            'label' => 'Related Terms',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_glossaryone_domain_model_term',
                'foreign_table_where' => 'ORDER BY tx_glossaryone_domain_model_term.uid',
                'MM' => 'tx_glossaryone_domain_model_term_related_mm',
                'MM_opposite_field' => 'related_from',
                'items' => [],
                'size' => 7,
                'minitems' => 0,
                'maxitems' => 100,
                'enableMultiSelectFilterTextfield' => TRUE,
            ],
        ],
        'related_from' => [
            'exclude' => true,
            'label' => 'Related from',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'foreign_table' => 'tx_glossaryone_domain_model_term',
                'allowed' => 'tx_glossaryone_domain_model_term',
                'size' => 5,
                'maxitems' => 100,
                'MM' => 'tx_glossaryone_domain_model_term_related_mm',
                'readOnly' => 1,
            ]
        ],
        'seo_title' => [
            'label' => 'SEO Title',
            'exclude' => 1,
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'max' => 200
            ]
        ],
        'seo_description' => [
            'label' => 'SEO Description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'max' => 400
            ]
        ],
        'seo_keywords' => [
            'label' => 'SEO Keywords',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 5,
                'placeholder' => 'Keyword1, Keyword2, Keyword3'
            ]
        ],
    ],
    'palettes' => [
        'paletteAbbrTitle' => [
            'showitem' => 'abbreviation, full_title',
        ],
        'paletteAccess' => [
            'showitem' => 'starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
					endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
					--linebreak--, fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
					--linebreak--,editlock,',
        ],
        'paletteCore' => [
            'showitem' => 'istopnews, type, sys_language_uid, hidden,',
        ],
    ],
    'types' => [
        '0' => [
            'showitem' => '
            title, 
            --palette--;;paletteCore,
            --palette--;Type;paletteAbbrTitle, 
            content, synonyms,
			
			--div--;Relations,related,related_from,
			
			--div--;SEO,seo_title,seo_description,seo_keywords,
			
			--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
				--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;paletteAccess'
        ]
    ]
];