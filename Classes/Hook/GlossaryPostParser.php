<?php

namespace Hn\GlossaryOne\Hook;


use Hn\GlossaryOne\Domain\Model\Synonym;
use Hn\GlossaryOne\Domain\Model\Term;
use Hn\PostParser\Service\PostParser;
use Hn\PostParser\Service\ReplacementProviderInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Fluid\Core\ViewHelper\TagBuilder;

class GlossaryPostParser implements ReplacementProviderInterface
{
    /**
     * @var array
     */
    protected $terms;

    /**
     * @var array
     */
    protected $settings;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    /**
     * @var \Hn\GlossaryOne\Domain\Repository\TermRepository
     * @inject
     */
    protected $termRepository;

    /**
     * @param PostParser $postParser
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    public function onPostParserExecution(PostParser $postParser)
    {
        /** @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager */
        $configurationManager = $this->objectManager->get(ConfigurationManager::class);
        $this->settings = $configurationManager->getConfiguration(ConfigurationManager::CONFIGURATION_TYPE_SETTINGS, 'glossaryone');
        $synonyms = [];
        $terms = [];

        if ($this->checkPagesRecursively('tx_glossaryone_exclude_parsing_recursively') || $this->settings['disableParsing'] || $GLOBALS['TSFE']->page['tx_glossaryone_exclude_parsing']) {
            return;
        }

        /** @var Term $term */
        foreach ($this->termRepository->findAll() as $term) {
            $terms[$term->getTitle()] = $term;

            /** @var Synonym $synonym */
            foreach ($term->getSynonyms() as $synonym) {
                $synonyms[$synonym->getName()] = $term;
            }
        }
        $this->terms = array_merge($terms, $synonyms);
        $postParser->registerTerms($this, array_keys($this->terms));
    }

    /**
     * @param string $term
     * @param \DOMNodeList $nodeList
     * @return array
     */
    public function getReplacements($term, \DOMNodeList $nodeList)
    {
        if (!array_key_exists($term, $this->terms)) {
            return [];
        }

        $allowedTags = GeneralUtility::trimExplode(',', $this->settings['allowedTags']);

        /** @var Term $termObject */
        $termObject = $this->terms[$term];
        $result = [];
        $replacedTerms = [];

        /** @var \DOMNode $node */
        foreach ($nodeList as $node) {
            /** @var \DOMElement $element */
            $element = $node->parentNode;
            if (
                !in_array($element->tagName, $allowedTags) ||
                $termObject->getUid() == $GLOBALS['_GET']['tx_glossaryone_glossary']['term'] ||
                ($this->isReplaceSingle() && in_array($term, $replacedTerms)) ||
                empty($this->getGlossaryUri())
            ) {
                $result[] = $term;
            } else {
                $result[] = $this->buildLink($termObject, $term);
                $replacedTerms[] = $term;
            }
        }

        return $result;
    }

    /**
     * @param Term $termObject
     * @param string $term
     * @return string
     */
    private function buildLink($termObject, $term)
    {
        $replacement = $term;

        if ($termObject->getAbbreviation()) {
            $abbr = new TagBuilder('abbr');
            $abbr->forceClosingTag(true);
            $abbr->addAttribute('title', $termObject->getFullTitle());
            $abbr->setContent($term);
            $replacement = $abbr->render();
        }

        $link = new TagBuilder('a');
        $link->addAttribute('class', $this->settings['linkClass']);
        $link->addAttribute('href', $this->getTermUri($termObject));
        $link->forceClosingTag(true);
        $link->setContent($replacement);
        $replacement = $link->render();

        return $replacement;
    }

    /**
     * @param Term $term
     * @return string
     */
    private function getTermUri(Term $term)
    {
        $uriBuilder = $this->objectManager->get(UriBuilder::class);

        $uri = $uriBuilder->reset()
            ->setTargetPageUid($this->settings['detailUid'])
            ->uriFor(
                'show',
                ['term' => $term,],
                'Term',
                'glossaryone',
                'Glossary');

        return htmlspecialchars($uri);
    }

    /**
     * @return string
     */
    protected function getGlossaryUri()
    {
        $uriBuilder = $this->objectManager->get(UriBuilder::class);

        $uri = $uriBuilder->reset()
            ->setTargetPageUid($this->settings['glossaryUid'])
            ->build();

        return $uri;
    }

    /**
     * @return bool
     */
    public function isReplaceSingle()
    {
        return ($this->settings['replaceSingle'] || $GLOBALS['TSFE']->page['tx_glossaryone_first_occurence'] || $this->checkPagesRecursively('tx_glossaryone_first_occurence_recursively')) && !$GLOBALS['TSFE']->page['tx_glossaryone_exclude_first_occurence'];
    }

    /**
     * @return bool
     */
    public function isReplaceSubstrings()
    {
        return $this->settings['replaceSubstrings'];
    }

    /**
     * @param string $key
     * @return bool
     */
    private function checkPagesRecursively($key)
    {
        foreach ($GLOBALS['TSFE']->rootLine as $page) {
            if($page[$key]) {
                return true;
            }
        }
    }
}