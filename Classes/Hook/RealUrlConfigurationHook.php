<?php

namespace Hn\GlossaryOne\Hook;


class RealUrlConfigurationHook
{
    /**
     * @param $params
     * @return mixed
     */
    public function add($params)
    {
        return array_merge_recursive($params['config'], [
                'postVarSets' => [
                    '_DEFAULT' => [
                        'term' => [
                            [
                                'GETvar' => 'tx_glossaryone_glossary[controller]',
                                'noMatch' => 'bypass',
                            ],
                            [
                                'GETvar' => 'tx_glossaryone_glossary[action]',
                                'noMatch' => 'bypass',
                            ],
                            [
                                'GETvar' => 'tx_glossaryone_glossary[term]',
                                'lookUpTable' => [
                                    'table' => 'tx_glossaryone_domain_model_term',
                                    'id_field' => 'uid',
                                    'alias_field' => 'title',
                                    'useUniqueCache' => 1,
                                    'useUniqueCache_conf' => [
                                        'strtolower' => 1,
                                        'spaceCharacter' => '-',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        );
    }
}