<?php

namespace Hn\GlossaryOne\Service;


use Hn\GlossaryOne\Domain\Model\Term;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

class GlossaryService
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    /**
     * @var \Hn\GlossaryOne\Domain\Repository\TermRepository
     * @inject
     */
    protected $termRepository;

    /**
     * @var array
     */
    protected $regexToGroupMap;

    /**
     * Find all terms grouped by first character
     *
     * @return array
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    public function getTermsGroupedByFirstCharacter()
    {
        $terms = $this->termRepository->findAll();

        $groupedTerms = [];

        foreach ($terms as $term) {

            $groupedTerms[$this->getGroupName($term)][] = $term;
        }

        return $groupedTerms;
    }

    /**
     * @param Term $term
     * @return int|mixed|null|string|string[]
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    protected function getGroupName(Term $term)
    {
        $firstChar = mb_strtoupper(mb_substr($term->getTitle(), 0, 1));


        foreach ($this->getRegexToGroupMap() as $name => $regex) {
            if (preg_match($regex, $firstChar)) {
                return $name;
            }
        }

        if (in_array($firstChar, $this->getGroupNames())) {
            return $firstChar;
        }

        return $this->getDefaultGroupName();
    }

    protected function getGroupNames()
    {
        return range('A', 'Z');
    }

    protected function getDefaultGroupName()
    {
        return '0-9';
    }

    /**
     * @return array
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    protected function getRegexToGroupMap()
    {
        if (!$this->regexToGroupMap) {

            /** @var ConfigurationManager $configurationManager */
            $configurationManager = $this->objectManager->get(ConfigurationManager::class);
            $settings = $configurationManager->getConfiguration(ConfigurationManager::CONFIGURATION_TYPE_SETTINGS, 'glossaryone');
            $this->regexToGroupMap = [];
            foreach ($settings['termGroups'] as $termGroup) {
                $this->regexToGroupMap[$termGroup['name']] = $termGroup['regex'];
            }
        }

        return $this->regexToGroupMap;
    }
}