<?php

namespace Hn\GlossaryOne\ViewHelpers;

use TYPO3\CMS\Frontend\ContentObject\RecordsContentObject;

/**
 * tt_content ViewHelper
 */
class ContentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    protected $escapeOutput = false;

    /**
     * Parse content element
     *
     * @param  int     UID of the CE
     * @return   string  Parsed CE
     */
    public function render($uid)
    {
        $conf = array(
            'tables' => 'tt_content',
            'source' => $uid,
            'dontCheckPid' => 1,
        );


        return $this->objectManager->get(RecordsContentObject::class)->render($conf);
    }
}