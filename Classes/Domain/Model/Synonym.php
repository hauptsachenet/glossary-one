<?php

namespace Hn\GlossaryOne\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Synonym extends AbstractEntity
{

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

}