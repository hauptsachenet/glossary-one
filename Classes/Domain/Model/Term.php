<?php

namespace Hn\GlossaryOne\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Term extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $fullTitle = '';

    /**
     * @var int
     */
    protected $abbreviation;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\GlossaryOne\Domain\Model\Content>
     */
    protected $content;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\GlossaryOne\Domain\Model\Synonym>
     */
    protected $synonyms;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Hn\GlossaryOne\Domain\Model\Term>
     * @lazy
     */
    protected $related;

    /**
     * @var int
     */
    protected $hidden;

    /**
     * @var string
     */
    protected $seoTitle;

    /**
     * @var string
     */
    protected $seoDescription;

    /**
     * @var string
     */
    protected $seoKeywords;

    /**
     * Term constructor.
     * @param string $title
     * @param string $fullTitle
     * @param int $abbreviation
     * @param Content[]|\TYPO3\CMS\Extbase\Persistence\ObjectStorage $content
     * @param Synonym[]|\TYPO3\CMS\Extbase\Persistence\ObjectStorage $synonyms
     * @param Term[]|\TYPO3\CMS\Extbase\Persistence\ObjectStorage $related
     * @param int $hidden
     * @param string $seoTitle
     * @param string $seoDescription
     * @param string $seoKeywords
     */
    public function __construct(string $title, string $fullTitle, int $abbreviation, $content, $synonyms, $related, int $hidden, string $seoTitle, string $seoDescription, string $seoKeywords)
    {
        $this->title = $title;
        $this->fullTitle = $fullTitle;
        $this->abbreviation = $abbreviation;
        $this->content = $content;
        $this->synonyms = $synonyms;
        $this->related = $related;
        $this->hidden = $hidden;
        $this->seoTitle = $seoTitle;
        $this->seoDescription = $seoDescription;
        $this->seoKeywords = $seoKeywords;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFullTitle(): string
    {
        return $this->fullTitle;
    }

    /**
     * @param string $fullTitle
     */
    public function setFullTitle(string $fullTitle): void
    {
        $this->fullTitle = $fullTitle;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage|Term[]
     */
    public function getSynonyms(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->synonyms;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getContent(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getAbbreviation(): int
    {
        return $this->abbreviation;
    }

    /**
     * @param int $abbreviation
     */
    public function setAbbreviation(int $abbreviation): void
    {
        $this->abbreviation = $abbreviation;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getRelated(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->related;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $related
     */
    public function setRelated(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $related): void
    {
        $this->related = $related;
    }

    /**
     * @return int
     */
    public function getHidden(): int
    {
        return $this->hidden;
    }

    /**
     * @return string
     */
    public function getSeoTitle(): string
    {
        return $this->seoTitle;
    }

    /**
     * @param string $seoTitle
     */
    public function setSeoTitle(string $seoTitle): void
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return string
     */
    public function getSeoDescription(): string
    {
        return $this->seoDescription;
    }

    /**
     * @param string $seoDescription
     */
    public function setSeoDescription(string $seoDescription): void
    {
        $this->seoDescription = $seoDescription;
    }

    /**
     * @return string
     */
    public function getSeoKeywords(): string
    {
        return $this->seoKeywords;
    }

    /**
     * @param string $seoKeywords
     */
    public function setSeoKeywords(string $seoKeywords): void
    {
        $this->seoKeywords = $seoKeywords;
    }
}