<?php

namespace Hn\GlossaryOne\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Hn\GlossaryOne\Domain\Model\Term;

class TermController extends ActionController
{
    /**
     * @var \Hn\GlossaryOne\Service\GlossaryService
     * @inject
     */
    protected $glossaryService;

    /**
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    public function listAction()
    {
        $terms = $this->glossaryService->getTermsGroupedByFirstCharacter();

        $this->view->assign('terms', $terms);
    }

    /**
     * Show detailed term action
     *
     * @param Term $term
     */
    public function showAction(Term $term)
    {
        if ($term->getSeoTitle()) {
            $GLOBALS['TSFE']->altPageTitle = $term->getSeoTitle();
        } else {
            $GLOBALS['TSFE']->altPageTitle = $term->getTitle();
        }

        $this->view->assign('term', $term);
    }
}