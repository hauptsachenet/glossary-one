<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'GlossaryOne',
    'description' => 'A glossary extension.',
    'category' => 'plugin',
    'author' => 'Marvin Dosse',
    'author_company' => 'hauptsache.net GmbH',
    'author_email' => 'marvin@hauptsache.net',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '1.1.6',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
            'post_parser' => '1.0.5-1.0.99'
        ]
    ]
];