# TYPO3 CMS Extension "GlossaryOne"

A TYPO3 glossary extension that uses content elements to describe terms. Furthermore 
it is able to parse pages to find terms and link them to the detailed view page.

## Installation

1.	Install and activate the extension in the Extension Manager
2.	Create a list page, a detail page (optional, you can also use the list page as detail page)
    and a folder, where the terms are stored
3.  Add the Glossary plugin to the list and detail page
3.	Add static TypoScript to your site template
4.	Use the constant editor on your template to configure necessary uids
5.  Create terms in the folder

## Screenshots    
![Edit constants](Documentation/Images/term.png "Edit constants")

![Add new term](Documentation/Images/constants.png "Add new term")

![Glossary](Documentation/Images/glossary.png "Glossary")

![Parsing terms](Documentation/Images/parsing.png "Parsing terms")

## Configuration

-	There are several options you can configure in the constants, e.g. define tags that shall be parsed.
    By default `<li>` and `<p>`-tags are parsed.
-	In the TypoScript setup.txt you can define how to handle umlauts.
    By default: Ä belongs to A, Ö belongs to O and Ü belongs to U.
    Moreover you can combine 2 or more letters.
    X and Y are combined by default, so words that start with X or Y belong to the same group.


```
termGroups {
    1 {
        name = A
        regex = /ä/iu
    }

    2 {
        name = O
        regex = /ö/iu
    }

    3 {
        name = U
        regex = /ü/iu
    }

    4 {
        name = XY
        regex = /x|y/i
    }
}
```


