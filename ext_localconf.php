<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Hn.GlossaryOne',
    'Glossary',
    [
        'Term' => 'list, show',
    ],
    [
        'Term' => 'list',
    ]
);


// RealURL configuration hook
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration'][$_EXTKEY] = \Hn\GlossaryOne\Hook\RealUrlConfigurationHook::class . '->add';

$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
    \Hn\PostParser\Service\PostParser::class,
    'onPostParserExecution',
    \Hn\GlossaryOne\Hook\GlossaryPostParser::class,
    'onPostParserExecution'
);


if (!empty($GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'])) {
    $GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ',';
}
$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= 'tx_glossaryone_exclude_parsing_recursively,tx_glossaryone_first_occurence_recursively';