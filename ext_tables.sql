CREATE TABLE tx_glossaryone_domain_model_term (
  uid int(11) NOT NULL AUTO_INCREMENT,
  l10n_state text COLLATE utf8_unicode_ci,
  pid int(11) NOT NULL DEFAULT '0',
  deleted smallint(6) NOT NULL DEFAULT '0',
  tstamp int(11) NOT NULL DEFAULT '0',
  crdate int(11) NOT NULL DEFAULT '0',
  cruser_id int(11) NOT NULL DEFAULT '0',
  origUid int(11) NOT NULL DEFAULT '0',
  sys_language_uid int(11) NOT NULL DEFAULT '0',
  l10n_source int(11) NOT NULL DEFAULT '0',
  l18n_diffsource mediumtext COLLATE utf8_unicode_ci,
  l18n_parent int(11) NOT NULL DEFAULT '0',
  hidden smallint(6) NOT NULL DEFAULT '0',
  starttime int(10) unsigned NOT NULL DEFAULT '0',
  endtime int(10) unsigned NOT NULL DEFAULT '0',
  fe_group varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  editlock smallint(6) NOT NULL DEFAULT '0',
  sorting int(11) NOT NULL DEFAULT '0',
  title varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  content smallint(5) unsigned NOT NULL DEFAULT '0',
  related int(11) NOT NULL DEFAULT '0',
	related_from int(11) DEFAULT '0' NOT NULL,
  synonyms smallint(5) unsigned NOT NULL DEFAULT '0',
  abbreviation smallint(6) NOT NULL DEFAULT '0',
  full_title varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  seo_title varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  seo_description text COLLATE utf8_unicode_ci,
  seo_keywords text COLLATE utf8_unicode_ci,
  PRIMARY KEY (uid),
  KEY pid (pid),
  KEY language (l18n_parent,sys_language_uid),
  KEY sorting (pid,sorting)
);

CREATE TABLE tx_glossaryone_domain_model_synonym (
  uid int(11) NOT NULL AUTO_INCREMENT,
  l10n_state text COLLATE utf8_unicode_ci,
  pid int(11) NOT NULL DEFAULT '0',
  deleted smallint(6) NOT NULL DEFAULT '0',
  tstamp int(11) NOT NULL DEFAULT '0',
  crdate int(11) NOT NULL DEFAULT '0',
  cruser_id int(11) NOT NULL DEFAULT '0',
  origUid int(11) NOT NULL DEFAULT '0',
  sys_language_uid int(11) NOT NULL DEFAULT '0',
  l10n_source int(11) NOT NULL DEFAULT '0',
  l18n_diffsource mediumtext COLLATE utf8_unicode_ci,
  l18n_parent int(11) NOT NULL DEFAULT '0',
  hidden smallint(6) NOT NULL DEFAULT '0',
  starttime int(10) unsigned NOT NULL DEFAULT '0',
  endtime int(10) unsigned NOT NULL DEFAULT '0',
  fe_group varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  editlock smallint(6) NOT NULL DEFAULT '0',
  sorting int(11) NOT NULL DEFAULT '0',
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  term_id int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (uid),
  KEY pid (pid),
  KEY language (l18n_parent,sys_language_uid),
  KEY sorting (pid,sorting),
  KEY term_id (term_id)
);

CREATE TABLE tx_glossaryone_domain_model_term_related_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
	sorting_foreign int(11) DEFAULT '0' NOT NULL,

  PRIMARY KEY (uid_local, uid_foreign),

  INDEX local (uid_local, sorting ASC),
);

CREATE TABLE tt_content (
    tx_glossaryone_term int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE pages (
  tx_glossaryone_exclude_parsing TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL,
  tx_glossaryone_exclude_parsing_recursively TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL,
  tx_glossaryone_first_occurence TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL,
  tx_glossaryone_first_occurence_recursively TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL,
  tx_glossaryone_exclude_first_occurence TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL
);